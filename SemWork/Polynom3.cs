﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Linq;
using System.Collections.Specialized;

namespace SemWork
{
    class Polynom3
    {
        public List<Polynom3> polynomial = new List<Polynom3>();

        public int coef; 

       public int deg_1;
        int deg_2;
        int deg_3;

        Polynom3(int coef, int deg1, int deg2, int deg3)
        {
            this.coef = coef;
            deg_1 = deg1;
            deg_2 = deg2;
            deg_3 = deg3;
        }

        public Polynom3(string FileName)
        {
            string full_text = File.ReadAllText(FileName).Replace("+", "");


            string[] split = full_text.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            foreach (var a in split)
            {
                int i = 0;
                string temp = "";
                int coeff = 1;
                int degree1 = 0;
                int degree2 = 0;
                int degree3 = 0;

//Проход до первого буквенного символа

                while (a[i] != 'x' && a[i] != 'y' && a[i] != 'z')
                {
                    temp += a[i];
                    i++;
                }
                if (temp != "")
                    coeff = int.Parse(temp);
                temp = "";

                while (i < a.Length)
                {
//Проход до первого циркумфлекса и его пропуск

                    if (full_text.Contains('^'))
                        while (a[i] != '^')
                            i++;
                    i += 1;
                    
                    int t = i;
                         
//Проход до первой попавшейся буквы. В зависимости от буквы будет определяться степень

                    while (i < a.Length && i != 'x' && a[i] != 'y' && a[i] != 'z')
                    {
                        temp += a[i];
                        i++;
                    }
                    if (a[t - 2] == 'x')
                        degree1 = int.Parse(temp);
                    if (a[t - 2] == 'y')
                        degree2 = int.Parse(temp);
                    else if (a[t - 2] == 'z') degree3 = int.Parse(temp);

                    temp = "";
                }

                polynomial.Add(new Polynom3(coeff, degree1, degree2, degree3));
            }

//Сортировка по "индексам" и степеням

                        polynomial = polynomial.Where(x => x.deg_1 != 0).OrderByDescending(x => x.deg_1)
                     .Union(polynomial.Where(x => x.deg_2 != 0).OrderByDescending(x => x.deg_2))
                     .Union(polynomial.Where(x => x.deg_3 != 0).OrderByDescending(x => x.deg_3)).ToList();

//Если встретятся подобные мономы (с одинаковыми "индексами" и степенями), то к одному моному мы прибавим подобный, а подобный удалим

            for(int i = 0; i < polynomial.Count - 1; i++)
                if (polynomial[i].deg_1 == polynomial[i+1].deg_1 & polynomial[i].deg_2 == polynomial[i + 1].deg_2 &
                    polynomial[i].deg_3 == polynomial[i + 1].deg_3)
                {
                    polynomial[i] = new Polynom3(polynomial[i].coef + polynomial[i + 1].coef, polynomial[i].deg_1, polynomial[i].deg_2,
                        polynomial[i].deg_3);
                    polynomial.RemoveAt(i + 1);
                }

        }


        public void Delete(int deg1, int deg2, int deg3)
        {
            for (int i = 0; i < polynomial.Count; i++)
                if (polynomial[i].deg_1 == deg1 || polynomial[i].deg_2 == deg1 || polynomial[i].deg_3 == deg1 ||
                   polynomial[i].deg_1 == deg2 || polynomial[i].deg_2 == deg2 || polynomial[i].deg_3 == deg2 ||
                   polynomial[i].deg_1 == deg3 || polynomial[i].deg_2 == deg3 || polynomial[i].deg_3 == deg3)
                {
                    polynomial.Remove(polynomial[i]);
                    i -= 1;
                }
        } 


        public int Value(int x, int y, int z)
        {
            double returned = 0;
            double sum = 0;

            foreach (var a in polynomial)
            {
                    if (a.deg_1 != 0 || a.deg_2 != 0 || a.deg_3 != 0)
                        sum += a.coef;

                    if(a.deg_1 != 0)
                        sum *= Math.Pow(x, a.deg_1);

                    if (a.deg_2 != 0)
                        sum *= Math.Pow(y, a.deg_2);

                    if (a.deg_3 != 0)
                        sum *= Math.Pow(z, a.deg_3);
                returned += sum;
                sum = 0;
            }

            return (int)returned;
        }

        public void Derivate(int x)
        {
            int helper = 0;
            if (polynomial[x].deg_1 != 0 & polynomial[x].deg_2 == 0 & polynomial[x].deg_3 == 0)
            {
                helper++;
                Console.WriteLine($"{polynomial[x].deg_1 * polynomial[x].coef}x^({polynomial[x].deg_1 - 1})");
            }


            if (polynomial[x].deg_2 != 0 & polynomial[x].deg_1 == 0 & polynomial[x].deg_3 == 0)
            {
                helper++;
                Console.WriteLine($"{polynomial[x].deg_2 * polynomial[x].coef}y^({polynomial[x].deg_2 - 1})");
            }

            if (polynomial[x].deg_3 != 0 & polynomial[x].deg_2 == 0 & polynomial[x].deg_1 == 0)
            {
                helper++;
                Console.WriteLine($"{polynomial[x].deg_3 * polynomial[x].coef}z^({polynomial[x].deg_3 - 1})");
            }

            if(helper == 0) 
                Console.WriteLine($"Производная будет браться по x: {polynomial[x].deg_1 * polynomial[x].coef}x^({polynomial[x].deg_1 - 1})" +
                 $"y^({polynomial[x].deg_2})z^({polynomial[x].deg_3})");
        }

        public void Add(Polynom3 p)
        {

 //Прежде чем добавлять элементы одного полинома в другой они проходят проверку на "подобность" и, если подобны, складываются (или отнимаются)

            if (p.polynomial != null)
            {
                for (int i = 0; i < p.polynomial.Count; i++)
                    if (polynomial[i].deg_1 == p.polynomial[i].deg_1 & polynomial[i].deg_2 == p.polynomial[i].deg_2
                       & polynomial[i].deg_3 == p.polynomial[i].deg_3)
                        polynomial[i] = new Polynom3(polynomial[i].coef + p.polynomial[i].coef, polynomial[i].deg_1,
                                polynomial[i].deg_2, polynomial[i].deg_3);
                        else polynomial.Add(p.polynomial[i]);

//Обычная сортировка по "индексам" и степеням
                polynomial = polynomial.Where(x => x.deg_1 != 0).OrderByDescending(x => x.deg_1)
                    .Union(polynomial.Where(x => x.deg_2 != 0).OrderByDescending(x => x.deg_2))
                    .Union(polynomial.Where(x => x.deg_3 != 0).OrderByDescending(x => x.deg_3)).ToList();

//Если встречаются подобные элементы (теперь уже из объедененного списка), то они складываются или вычитаются

                for (int i = 0; i < polynomial.Count - 1; i++)
                    if (polynomial[i].deg_1 == polynomial[i + 1].deg_1 & polynomial[i].deg_2 == polynomial[i + 1].deg_2 &
                        polynomial[i].deg_3 == polynomial[i + 1].deg_3)
                    {
                        polynomial[i] = new Polynom3(polynomial[i].coef + polynomial[i + 1].coef, polynomial[i].deg_1, polynomial[i].deg_2,
                            polynomial[i].deg_3);
                        polynomial.RemoveAt(i + 1);
                    }
            }
        }

        public int[] MinCoef()
        {
            int[] returned = new int[3] {polynomial[0].deg_1, polynomial[0].deg_2, polynomial[0].deg_3 };

            var min = polynomial[0].coef;

            foreach (var a in polynomial)
                if (min > a.coef)
                {
                    min = a.coef;
                    returned[0] = a.deg_1;
                    returned[1] = a.deg_2;
                    returned[2] = a.deg_3;
                }

              return returned;
        }

        public void Insert(int coef, int deg1, int deg2, int deg3)
        {
            for (int i = 0; i < polynomial.Count; i++)
                if (polynomial[i].deg_1 == deg1 & polynomial[i].deg_2 == deg2 & polynomial[i].deg_3 == deg3)
                {
                    polynomial[i] = new Polynom3(coef, deg1, deg2, deg3);
                    return;
                }
                else polynomial.Add(new Polynom3(coef, deg1, deg2, deg3));
        }

        public override string ToString()
        {
            string returned = "";
            if (polynomial != null) 
            {
                foreach (var a in polynomial)
                {
                        returned += $"{a.coef}";
                    if (a.deg_1 != 0)
                        returned += $"x^({a.deg_1})";
                    if (a.deg_2 != 0)
                        returned += $"y^({a.deg_2})";
                    if (a.deg_3 != 0)
                        returned += $"z^({a.deg_3})";

                    returned += " + ";
                }
                returned = returned.Substring(0, returned.LastIndexOf("+"));
            }

            return returned;
        }
    }
}
