﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System.Reflection;

namespace SemWork
{
    class Program
    {
        static void Main(string[] args)
        {
            var pol = new Polynom3(@"C:\Users\SCVRLET\Desktop\Polinom.txt");
              var pol_2 = new Polynom3(@"C:\Users\SCVRLET\Desktop\Polinom2.txt");
            Console.WriteLine(pol + " - pol");
            Console.WriteLine(pol_2 + " - pol_2");
            pol.Add(pol_2);
            Console.WriteLine(pol);
            Console.WriteLine(pol.Value(1, 1, 1));
        }
    }
}
